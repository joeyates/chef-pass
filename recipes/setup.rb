pass_package

pass_setup do
  user node["pass"]["user"]
  group node["pass"]["group"]
  gpg_id node["pass"]["gpg_id"]
end

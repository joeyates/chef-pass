# pass cookbook

Installs and configures the pass password manager.

# Recipes

## pass_setup

Initializes pass storage for a user.

Required attributes:

* user
* group
* gpg_id

resource_name :pass_package

actions :install
default_action :install

property :name, String, default: "default"

action :install do
  package "tree"
  package "pass"
end

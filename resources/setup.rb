resource_name :pass_setup

actions :create
default_action :create

property :user, String
property :group, String
property :gpg_id, String

action :create do
  execute "initialize pass" do
    user_home = ::File.join("", "home", new_resource.user) # TODO

    command "pass init #{new_resource.gpg_id}"
    cwd user_home
    environment(
      "PASSWORD_STORE_DIR" => ::File.join(user_home, ".password-store"),
      "GNUPGHOME" => ::File.join(user_home, ".gnupg")
    )
    user new_resource.user
    group new_resource.group
    not_if { false } # TODO
  end
end
  
